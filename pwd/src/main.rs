extern crate clap;
use clap::{Arg, App};
use std::{env, process, path};

fn main() {
    let matches = App::new("pwd")
        .version("0.1.0")
        .author("luc65r <lucas@ransan.tk>")
        .about("Print the full filename of the current working directory.")
        .arg(Arg::with_name("logical")
            .short("L")
            .long("logical")
            .help("Use PWD from environment, even if it contains symlinks"))
        .arg(Arg::with_name("physical")
            .short("P")
            .long("physical")
            .help("Avoid all symlinks")
            .overrides_with("logical"))
        .get_matches();

    if !matches.is_present("physical") {
        match env::var_os("PWD") {
            Some(pwd) => {
                if path::PathBuf::from(&pwd).is_dir() {
                    println!("{}", pwd.to_str().unwrap());
                    process::exit(0);
                }
            },
            None => (),
        }
    }
    match env::current_dir() {
        Ok(pwd) => {
            println!("{}", pwd.to_str().unwrap());
        },
        Err(e) => {
            eprintln!("pwd: {}", e);
            process::exit(1);
        }
    }
}
