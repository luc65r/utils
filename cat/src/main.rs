extern crate clap;
use clap::{Arg, App};
use std::{fs, io::{self, ErrorKind}, process};

fn main() {
    let matches = App::new("cat")
        .version("0.1.0")
        .author("luc65r <lucas@ransan.tk>")
        .about("Concatenate FILES to standard output.")
        .arg(Arg::with_name("u")
            .short("u")
            .help("(ignored)"))
        .arg(Arg::with_name("FILES")
            .multiple(true))
        .get_matches();

    match matches.values_of("FILES") {
        Some(files) => for file in files {
            if file == "-" {
                io::copy(&mut io::stdin(), &mut io::stdout()).unwrap();
            } else {
                io::copy(&mut fs::File::open(file).unwrap_or_else(|err| {
                    eprintln!("cat: {}: {}", file, match err.kind() {
                        ErrorKind::NotFound => "No such file or directory",
                        ErrorKind::PermissionDenied => "Permission denied",
                        _ => "Unknown error"
                    });
                    process::exit(1);
                }), &mut io::stdout()).unwrap();
            }
        },
        None => {
            io::copy(&mut io::stdin(), &mut io::stdout()).unwrap();
        }
    }
}
